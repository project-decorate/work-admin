package com.decorate.workadmin.domain;

import java.io.Serializable;

/**
 * 公司表(基层单位)(Company)实体类
 *
 * @author LCF
 * @since 2020-11-10 14:01:56
 */
public class Company implements Serializable {
    private static final long serialVersionUID = -68736085423080513L;

    private Long id;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}