package com.decorate.workadmin.utils;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CommonUtils {


    /**
     * 格式化金额
     *
     * @param decimal
     * @return
     */
    public static String formatMoney(BigDecimal decimal) {
        decimal = (decimal == null) ? BigDecimal.ZERO : decimal;
        return formatMoney(decimal, 2);
    }


    /**
     * 格式化费率
     *
     * @param decimal
     * @return
     */
    public static String formatPercent(BigDecimal decimal) {
        decimal = (decimal == null) ? BigDecimal.ZERO : decimal;
        return formatPercent(decimal, 2);
    }

    /**
     * 格式化费率
     *
     * @param decimal
     * @return
     */
    public static String formatPercent(BigDecimal decimal, int scale) {
        return formatPercentNum(decimal, scale) + "%";
    }

    /**
     * 格式化百分比数字(不带百分号)
     *
     * @param decimal
     * @return
     */
    public static String formatPercentNum(BigDecimal decimal, int scale) {
        if (decimal == null) {
            return "0.00";
        }
        BigDecimal num = decimal.multiply(new BigDecimal("100")).setScale(scale, RoundingMode.HALF_UP);
        return String.valueOf(num);
    }

    /**
     * 格式化数字
     *
     * @param decimal
     * @param scale
     * @return
     */
    public static String formatMoney(BigDecimal decimal, int scale) {
        if (decimal == null) {
            return "";
        }
        return String.valueOf(decimal.setScale(scale, RoundingMode.HALF_UP));
    }

    /**
     * 身份证号取男女
     *
     * @param identityNo
     * @return
     */
    public static String getIdentityGender(String identityNo) {
        if (StringUtils.isBlank(identityNo) || identityNo.length() != 18) {
            return null;
        }
        Integer val = Integer.parseInt(String.valueOf(identityNo.charAt(16)));
        return (val % 2 == 0) ? "女" : "男";
    }

    /**
     * 身份证号取生日
     *
     * @param identityNo
     * @return
     */
    public static String getIdentityBirth(String identityNo) {
        if (StringUtils.isBlank(identityNo) || identityNo.length() != 18) {
            return null;
        }
        return identityNo.substring(6, 14);
    }

    /**
     * 查询参数
     *
     * @param paramMap
     * @param name
     * @return String
     */
    public static String getValue(Map<String, Object> paramMap, String name) {
        if (paramMap == null || paramMap.isEmpty() || StringUtils.isBlank(name) || !paramMap.containsKey(name)) {
            return null;
        }
        Object obj = paramMap.get(name);
        if (obj == null) {
            return null;
        }
        String value = obj.toString();
        return value.trim();
    }

    public static String getValue(Map<String, Object> paramMap, String name, String defaultValue) {
        return CommonUtils.getValue(getValue(paramMap, name), defaultValue);
    }

    /**
     * 获取Integer的字符串类型
     *
     * @param value
     * @return
     */
    public static String getValue(Integer value) {
        if (value == null) {
            return "";
        }
        return String.valueOf(value);
    }

    /**
     * 获取Long的字符串
     *
     * @param value
     * @return
     */
    public static String getValue(Long value) {
        if (value == null) {
            return "";
        }
        return String.valueOf(value);
    }

    /**
     * 获取String的trimValue
     *
     * @param value
     * @return
     */
    public static String getValue(String value) {
        if (StringUtils.isBlank(value)) {
            return "";
        }
        return value.trim();
    }

    /**
     * 获取Decimal默认值
     *
     * @param applyAmount
     * @return
     */
    public static BigDecimal getDecimal(BigDecimal applyAmount) {
        return applyAmount == null ? BigDecimal.ZERO : applyAmount;
    }

    /**
     * 如果字符串为空返回默认值
     *
     * @param value
     * @param defaultValue
     * @return
     */
    public static String getValue(String value, String defaultValue) {
        return StringUtils.isBlank(value) ? defaultValue : value;
    }

    /**
     * 截取尾数字符
     *
     * @param value
     * @param len
     * @return
     */
    public static String getLast(String value, int len) {
        if (StringUtils.isBlank(value) || len <= 0 || value.length() - len <= 0) {
            return getValue(value);
        }
        return value.substring(value.length() - len, value.length());
    }

    /**
     * <pre>
     * 字符串替换显示*
     *
     * 比如身份证要显示成：370************013
     *
     * &#64;param value   - 要操作的内容
     * &#64;param start   - 替换开始位置
     * &#64;param end     - 替换结束位置
     * &#64;param repChar - 要替换成的内容
     * &#64;return String
     * </pre>
     */
    public static String replace(String value, int start, int end, String repChar) {
        if (StringUtils.isBlank(value) || start > end || repChar == null || value.length() < end) {
            return value;
        }
        String con = value.substring(start, end);
        String repStr = StringUtils.repeat(repChar, con.length());
        return value.replace(con, repStr);
    }



    /**
     * 计算百分比
     *
     * @param numOne
     * @param numTwo
     * @return
     */
    public static String calPercent(long numOne, long numTwo, int scale) {
        if (numTwo == 0) {
            return String.valueOf(BigDecimal.ZERO.setScale(scale)) + "%";
        }
        BigDecimal start = BigDecimal.valueOf(numOne);
        BigDecimal end = BigDecimal.valueOf(numTwo);
        BigDecimal result = start.multiply(new BigDecimal(100)).divide(end, RoundingMode.HALF_UP).setScale(scale);
        return String.valueOf(result) + "%";
    }

    /**
     * 是否小于等于
     *
     * @param valOne
     * @param valTwo
     * @return boolean
     */
    public static boolean isLessEqThan(BigDecimal valOne, BigDecimal valTwo) {
        BigDecimal valueOne = getDecimal(valOne);
        BigDecimal valueTwo = getDecimal(valTwo);
        return valueOne.compareTo(valueTwo) <= 0;
    }

    /**
     * 是否包含
     *
     * @param value
     * @param chars
     * @return boolean
     */
    public static boolean contains(String value, String chars) {
        if (StringUtils.isAnyBlank(value, chars)) {
            return false;
        }
        return value.contains(chars);
    }

    /**
     * 是否包含任一
     *
     * @param value
     * @param chars
     * @return
     */
    public static boolean containsAny(String value, String... chars) {
        if (chars == null || chars.length == 0) {
            return false;
        }
        boolean result = false;
        for (String content : chars) {
            if (contains(value, content)) {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * Get Int
     *
     * @param value
     * @return
     */
    public static Integer getInt(String value) {
        if (!StringUtils.isNumeric(value)) {
            return 0;
        }
        return Integer.parseInt(value.trim());
    }

    /**
     * 随机码
     *
     * @return String
     */
    public static String getRandom() {
        return String.valueOf(Math.random());
    }


    /**
     * 是否为零
     *
     * @param value
     * @return boolean
     */
    public static final boolean isZero(BigDecimal value) {
        return value == null || BigDecimal.ZERO.compareTo(value) == 0;
    }

    /**
     * 字符串左补齐
     *
     * @param value
     * @param length
     * @param sign
     * @return
     */
    public static String leftPad(String value, int length, String sign) {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        if (value.trim().length() >= length) {
            return value;
        }
        int padLen = length - value.trim().length();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < padLen; i++) {
            builder.append(sign);
        }
        return builder.toString() + value;
    }

    public static final boolean isAnySame(Integer expectCount, String... values) {
        Set<String> set = new HashSet<String>();
        set.addAll(Arrays.asList(values));
        return set.size() != expectCount;
    }

    /**
     * 计算两个日期之间相差的天数
     *
     * @param smdate
     *            较小的时间
     * @param bdate
     *            较大的时间
     * @return 相差天数
     * @throws ParseException
     */
    public static int daysBetween(Date smdate, Date bdate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        smdate = sdf.parse(sdf.format(smdate));
        bdate = sdf.parse(sdf.format(bdate));
        Calendar cal = Calendar.getInstance();
        cal.setTime(smdate);
        long time1 = cal.getTimeInMillis();
        cal.setTime(bdate);
        long time2 = cal.getTimeInMillis();
        long between_days = (time2 - time1) / (1000 * 3600 * 24);

        return Integer.parseInt(String.valueOf(between_days));
    }

}
