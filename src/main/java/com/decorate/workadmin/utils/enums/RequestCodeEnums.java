package com.decorate.workadmin.utils.enums;

/**
 * 请求状态码
 *
 * @date : 2020/11/12
 */
public enum RequestCodeEnums {
    SUCCESS("0000", "请求成功"),
    FAILED("C001", "请求失败"),
    LOGIN_EXPIRE("C002", "登录超时，请重新登录"),
    PARAM_ERROR("C003", "参数传递错误"),
    BUSINESS_FAILED("B001", "业务异常");

    private String code;
    private String msg;

    RequestCodeEnums(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getMsg() {
        return this.msg;
    }

    public String getCode() {
        return this.code;
    }

    public static String getMsg(String code) {
        for (RequestCodeEnums ele : values()) {
            if (ele.getCode().equals(code)) {
                return ele.getMsg();
            }
        }
        return null;
    }
}
