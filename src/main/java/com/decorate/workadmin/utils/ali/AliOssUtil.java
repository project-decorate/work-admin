package com.decorate.workadmin.utils.ali;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.CannedAccessControlList;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.UUID;

/**
 * OSS上传工具
 *
 * @author LCF
 */
@Component("AliOssUtil")
public class AliOssUtil {

    private static String endpoint;

    private static String accessKeyId;

    private static String accessKeySecret;

    private static String bucketName;

    private static String serviceName;

    private static String cdnUrl;

    @Value("${aliyun.oss.endpoint}")
    public void setEndpoint(String endpoint) {
        AliOssUtil.endpoint = endpoint;
    }

    @Value("${aliyun.oss.accessKeyId}")
    public void setAccessKeyId(String accessKeyId) {
        AliOssUtil.accessKeyId = accessKeyId;
    }

    @Value("${aliyun.oss.accessKeySecret}")
    public void setAccessKeySecret(String accessKeySecret) {
        AliOssUtil.accessKeySecret = accessKeySecret;
    }

    @Value("${aliyun.oss.bucketName}")
    public void setBucketName(String bucketName) {
        AliOssUtil.bucketName = bucketName;
    }

    @Value("${aliyun.oss.serviceName}")
    public void setServiceName(String serviceName) {
        AliOssUtil.serviceName = serviceName;
    }

    @Value("${aliyun.oss.cdn.url}")
    public void setCdnUrl(String cdnUrl) {
        AliOssUtil.cdnUrl = cdnUrl;
    }

    private static OSS ossClient = null;

    private AliOssUtil() {
    }

    private synchronized static OSS getInstance() {
        if (ossClient == null) {
            ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        }
        return ossClient;
    }

    /**
     * 文件上传
     *
     * @param destFilepath 上传目录
     * @param file         MultipartFile
     * @param fileName     文件名称
     * @return 文件访问地址
     */
    public static String uploadMultipartFile2Oss(MultipartFile file, String destFilepath, String fileName) throws Exception {
        if (StringUtils.isBlank(fileName)) {
            fileName = file.getOriginalFilename();
        }
        return uploadFile2Oss(file.getInputStream(), destFilepath, fileName);
    }

    /**
     * 上传到OSS服务器  如果同名文件会覆盖服务器上的
     *
     * @param instream     文件流
     * @param fileName     文件名称 包括后缀名
     * @param destFilepath 目标文件夹
     * @return 文件访问地址
     */
    public static String uploadFile2Oss(InputStream instream, String destFilepath, String fileName) {
        String resultStr = "";
        try {
            OSS ossClient = getInstance();
            //创建上传Object的Metadata
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentLength(instream.available());
            objectMetadata.setCacheControl("no-cache");
            objectMetadata.setHeader("Pragma", "no-cache");
            objectMetadata.setContentType(getcontentType(fileName.substring(fileName.lastIndexOf("."))));
            objectMetadata.setContentDisposition("inline;filename=" + fileName);
            //直接上传文件
            //ossClient.putObject(bucketName, destFilepath + fileName, instream, objectMetadata);

            // 设置访问权限上传
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, destFilepath + fileName, instream, objectMetadata);
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setObjectAcl(CannedAccessControlList.PublicRead);
            putObjectRequest.setMetadata(metadata);
            ossClient.putObject(putObjectRequest);

            resultStr = cdnUrl + "/" + destFilepath + fileName;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            try {
                if (instream != null) {
                    instream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return resultStr;
    }

    /**
     * 上传文件
     *
     * @param file
     * @param destFilepath 目标目录
     * @param fileName     文件名称
     * @return 文件访问地址
     */
    public static String uploadFile(File file, String destFilepath, String fileName) throws Exception {
        String resultStr = "";
        InputStream fin = new FileInputStream(file);
        if (StringUtils.isBlank(fileName)) {
            fileName = file.getName();
        }
        resultStr = uploadFile2Oss(fin, destFilepath, fileName);
        return resultStr;
    }

    /**
     * 上传byte图片
     *
     * @param imageBytes   byte数组格式图片
     * @param destFilepath 目标目录
     * @param imgName      图片名称
     * @return 图片访问地址
     */
    public static String uploadImgByte2Oss(byte[] imageBytes, String destFilepath, String imgName) throws Exception {
        String resultStr = "";
        if (StringUtils.isBlank(imgName)) {
            imgName = getRadomString() + ".png";
        }
        if (destFilepath.startsWith("/")) {
            destFilepath = destFilepath.substring(1);
        }
        OSS ossClient = getInstance();
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(imageBytes);
        // 直接上传
        // ossClient.putObject(bucketName, destFilepath + imgName, byteArrayInputStream);

        // 设置访问权限上传
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, destFilepath + imgName, byteArrayInputStream);
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setObjectAcl(CannedAccessControlList.PublicRead);
        putObjectRequest.setMetadata(metadata);
        ossClient.putObject(putObjectRequest);

        resultStr = cdnUrl + "/" + destFilepath + imgName;
        return resultStr;
    }

    /**
     * 生成随机数
     *
     * @return
     */
    private static String getRadomString() {
        String fileName;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(UUID.randomUUID().toString());
        stringBuilder.append(System.currentTimeMillis());
        fileName = stringBuilder.toString();
        return fileName;
    }

    /**
     * Description: 判断OSS服务文件上传时文件的contentType
     *
     * @param FilenameExtension 文件后缀
     * @return String
     */
    public static String getcontentType(String FilenameExtension) {
        if (FilenameExtension.equalsIgnoreCase(".bmp")) {
            return "image/bmp";
        }
        if (FilenameExtension.equalsIgnoreCase(".gif")) {
            return "image/gif";
        }
        if (FilenameExtension.equalsIgnoreCase(".jpeg") ||
                FilenameExtension.equalsIgnoreCase(".jpg") ||
                FilenameExtension.equalsIgnoreCase(".png")) {
            return "image/jpeg";
        }
        if (FilenameExtension.equalsIgnoreCase(".html")) {
            return "text/html";
        }
        if (FilenameExtension.equalsIgnoreCase(".txt")) {
            return "text/plain";
        }
        if (FilenameExtension.equalsIgnoreCase(".vsd")) {
            return "application/vnd.visio";
        }
        if (FilenameExtension.equalsIgnoreCase(".pptx") ||
                FilenameExtension.equalsIgnoreCase(".ppt")) {
            return "application/vnd.ms-powerpoint";
        }
        if (FilenameExtension.equalsIgnoreCase(".docx") ||
                FilenameExtension.equalsIgnoreCase(".doc")) {
            return "application/msword";
        }
        if (FilenameExtension.equalsIgnoreCase(".xml")) {
            return "text/xml";
        }
        return "image/jpeg";
    }

    /**
     * 测试
     */
    public static void test() {
        try {
            String str = AliOssUtil.uploadFile(new File("D:\\work\\data\\2.jpg"), "images/", "");
            System.out.println(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
