package com.decorate.workadmin.utils.redis;

import com.decorate.workadmin.utils.ToolSpring;
import com.decorate.workadmin.utils.redis.RedisClient;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * redis
 * @date : 2020/11/10
*/
public class RedisManager {

    @Autowired
    private RedisClient redisClient;

    private RedisClient getRedisClient() {
        if (null == this.redisClient) {
            this.redisClient = (RedisClient) ToolSpring.getBean("RedisClient");
        }
        return redisClient;
    }

    private static RedisManager instance = null;

    static {
        instance = new RedisManager();
    }

    private RedisManager() {}
    private static RedisManager getInstance() {
        return instance;
    }
    public static RedisClient getRedis(){
        return getInstance().getRedisClient();
    }

}
