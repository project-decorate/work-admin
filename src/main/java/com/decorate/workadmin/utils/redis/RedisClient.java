package com.decorate.workadmin.utils.redis;


import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Component("RedisClient")
public class RedisClient {
	private static String host;

	private static String port;

	private static String password;

	@Value("${redis.host}")
	public void setHost(String host) {
		RedisClient.host = host;
	}

	@Value("${redis.port}")
	public void setPort(String port) {
		RedisClient.port = port;
	}

	@Value("${redis.password}")
	public void setPassword(String password) {
		RedisClient.password = password;
	}

	//默认保存时间
	private static final int EXPIRE_DEFAULT_TIME = 60*30;
	//等待连接时间
	private static final int TIMEOUT = 10000;
	
	private static final String SET_SUCCESS = "OK";
	
	private static final Long RELEASE_SUCCESS = 1L;
	
	private static final Integer lockExpirseTime = 25;
	private static final Integer tryExpirseTime = 25;
	
	private DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");

	//是否设置有效期标识
	private int noSetExpFlag = 1;

	public static String type_str = "string";
	public static String type_obj = "object";
	public static String type_list = "list";
	public static String type_map = "map";

	//连接池
	private static JedisPool jedisPool = null;

	private synchronized static void initRedis(){
		//todo jedis优化配置
		JedisPoolConfig config = new JedisPoolConfig();
		config.setMaxTotal(300);
		config.setMaxIdle(10);
		config.setMinIdle(5);//设置最小空闲数
		config.setMaxWaitMillis(10000);
		config.setTestOnBorrow(true);
		config.setTestOnReturn(true);
		//Idle时进行连接扫描
		config.setTestWhileIdle(true);
		//表示idle object evitor两次扫描之间要sleep的毫秒数
		config.setTimeBetweenEvictionRunsMillis(30000);
		//表示idle object evitor每次扫描的最多的对象数
		config.setNumTestsPerEvictionRun(10);
		//表示一个对象至少停留在idle状态的最短时间，然后才能被idle object evitor扫描并驱逐；这一项只有在timeBetweenEvictionRunsMillis大于0时才有意义
		config.setMinEvictableIdleTimeMillis(60000);
		jedisPool = new JedisPool(config,host,Integer.parseInt(port),TIMEOUT,password);
	}

	protected synchronized static Jedis getJedis(){
		if(null == jedisPool) {
			initRedis();
		}
		return jedisPool.getResource();
	}

	private static void closeJedis(Jedis jedis){
		if(null != jedis){
			jedis.close();
		}
	}

	/**
	 * 存入 String
	 */
	public void set(String key, String value){
		Jedis jedis = getJedis();
		jedis.setex(key, EXPIRE_DEFAULT_TIME, value);
		closeJedis(jedis);
	}
	
	public void set(String key, String value, int seconds){
		Jedis jedis = getJedis();
		jedis.setex(key, seconds, value);
		closeJedis(jedis);
	}


	/**
	 * 存入 object
	 * @param key
	 */
	public <T> void set(String key, T t){
		this.set(key, t,EXPIRE_DEFAULT_TIME);
	}
	public <T> void set(String key, T t, int seconds){
		Jedis jedis = getJedis();
		ObjectsTranscoder<T> objectsTranscoder=new ObjectsTranscoder<>();
		byte[] bytes=objectsTranscoder.serialize(t);
		jedis.setex(key.getBytes(),seconds,bytes);
		closeJedis(jedis);
	}
	public <T> void setNoEx(String key, T t){
		Jedis jedis = getJedis();
		ObjectsTranscoder<T> objectsTranscoder=new ObjectsTranscoder<>();
		byte[] bytes=objectsTranscoder.serialize(t);
		jedis.set(key.getBytes(),bytes);
		closeJedis(jedis);
	}
	
	/**
	 * 存入 list.Object
	 * @param key
	 * @param list
	 */
	public <T> void set(String key, List<T> list){
		Jedis jedis = getJedis();
		ListTranscoder<Object> listTranscoder=new ListTranscoder<>();
		byte[] bytes=listTranscoder.serialize(list);
		jedis.setex(key.getBytes(),EXPIRE_DEFAULT_TIME,bytes);
		closeJedis(jedis);
	}
	public <T> void set(String key, List<T> list, int seconds){
		Jedis jedis = getJedis();
		ListTranscoder<Object> listTranscoder = new ListTranscoder<>();
		byte[] bytes=listTranscoder.serialize(list);
		jedis.setex(key.getBytes(),seconds,bytes);
		closeJedis(jedis);
	}

	/**
	 * 存入 map (string , object)
	 * @param key
	 * @param map
	 */
	public <T> void set(String key, Map<String, T> map){
		Jedis jedis = getJedis();
		MapTranscoder<T> mapTranscoder=new MapTranscoder<>();
		byte[] bytes=mapTranscoder.serialize(map);
		jedis.setex(key.getBytes(),EXPIRE_DEFAULT_TIME,bytes);
		closeJedis(jedis);
	}
	public <T> void set(String key, Map<String, T> map, int seconds){
		Jedis jedis = getJedis();
		MapTranscoder<T> mapTranscoder = new MapTranscoder<>();
		byte[] bytes = mapTranscoder.serialize(map);
		jedis.setex(key.getBytes(),seconds,bytes);
		closeJedis(jedis);
	}
	
	/**
	 * 读取 String
	 * @param key
	 * @return String
	 */
	public String getString(String key){
		Jedis jedis = getJedis();
		String value = jedis.get(key);
		closeJedis(jedis);
		return value;
	}
	
	/**
	 * 读取 Object
	 * @param key
	 * @return object
	 */
	public <T> T getObject(String key){
		Jedis jedis = getJedis();
		ObjectsTranscoder<T>  objectsTranscoder=new ObjectsTranscoder<>();
		T value=objectsTranscoder.deserialize(jedis.get(key.getBytes()));
		closeJedis(jedis);
		return value;
	}
	
	/**
	 * 读取 List
	 * @param key
	 * @return list
	 */
	public <T> List<T> getList(String key){
		Jedis jedis = getJedis();
		ListTranscoder<T> listTranscoder = new ListTranscoder<T>();
		List<T> value=listTranscoder.deserialize(jedis.get(key.getBytes()));
		closeJedis(jedis);
		return value;
	}


	
	/**
	 * 读取 Map
	 * @param key
	 * @return map
	 */
	public <T> Map<String, T> getMap(String key){
		Jedis jedis = getJedis();
		MapTranscoder<T> mapTranscoder=new MapTranscoder<>();
		Map<String, T> value=mapTranscoder.deserialize(jedis.get(key.getBytes()));
		closeJedis(jedis);
		return value;
	}

	/**
	 * @Title: 判断key是否存在
	 * @author jianghaoming
	 * @date 2017/1/13  14:28
	 */
	public Boolean exists(String key){
		Jedis jedis = getJedis();
		boolean flag = jedis.exists(key);
		closeJedis(jedis);
		return flag;
	}

	/**
	 * @Title: 检查是否为空
	 * @author jianghaoming
	 * @date 2017/1/13  14:33
	 */
	public void checkNull(String key) throws Exception
	{
		if(!exists(key)){
			throw new Exception("未找到相关redis数据，key="+key);
		}
	}

	/**
	 * @Title: 获取key的有效期
	 * @author jianghaoming
	 * @date 2017/1/13  14:29
	 * @return 单位为秒 , -1, 永不过期。
	 */
	public int getTtl(String key) throws Exception {
		checkNull(key);
		Jedis jedis = getJedis();
		int result = Math.toIntExact(jedis.ttl(key));
		closeJedis(jedis);
		return result;
	}



	/**
	 * @Title: list 添加一条数据
	 * @author jianghaoming
	 * @date 2017/1/13  14:19
	 * @param
	 * @return
	 */
	public <T> void insertList(String key, T t) throws Exception {
		this.insertList(key,t,noSetExpFlag);
	}
	public <T> void insertList(String key, T t, int seconeds) throws Exception {
		checkNull(key); //检查是否存在
		//获取redis中list，并添加数据
		List<T> list = this.getList(key);
		list.add(t);
		//设置有效期
		this.setTtl(key,list,seconeds,type_list);
	}

	/**
	 * @Title: map 添加一条数据
	 * @author jianghaoming
	 * @date 2017/1/13  14:19
	 * @param
	 * @return
	 */
	public <T> void insertMap(String key, String mapkey, T t) throws Exception {
		this.insertMap(key,mapkey,t,noSetExpFlag);
	}
	public <T> void insertMap(String key, String mapkey, T t,int seconeds) throws Exception {
		checkNull(key); //检查是否存在
		//获取redis中map，并添加数据
		Map<String,T> map = this.getMap(key);
		map.put(mapkey,t);
		//设置有效期
		this.setTtl(key,map,noSetExpFlag,type_map);
	}


	/**
	 * 删除
	 */
	public void delKey(String key){
		if( StringUtils.isBlank(key) ) return;
		Jedis jedis = getJedis();
		jedis.del(key);
		closeJedis(jedis);
	}
	
	
	/**
	 * 清空DB
	 */
	 public void flushDB(){
		 Jedis jedis = getJedis();
		 jedis.flushDB();
		 closeJedis(jedis);
	 }
	 
	/**
	 * 清空所有
	 */
	 public void flushAll(){
		 Jedis jedis = getJedis();
		 jedis.flushAll();
		 closeJedis(jedis);
	 }
	

	/**
	 * @Title: 判断有效期，并重新赋值
	 * @author jianghaoming
	 * @date 2017/1/13  15:06
	 * @param secd 有效期， 重置有效期时间，为1不设置有效期时间
	 */
	 @SuppressWarnings("unchecked")
	private <T> void setTtl(final String key, T t, int secd, final String type) throws Exception {
		 Map<String, T> map = null;
		 List<T> list = null;
		 if(type.equals(type_map)) {
			 map = (Map<String, T>) t;
		 }else if(type.equals(type_list)){
			 list = (List<T>) t;
		 }

		 if(secd != noSetExpFlag){
			 if(type.equals(type_map)) {
				 this.set(key, map, secd);
			 }else if(type.equals(type_list)){
				 this.set(key, list, secd);
			 }
		 } else {
			 secd = getTtl(key);
			 if (secd > 0) {
				 if(type.equals(type_map)) {
					 this.set(key, map, secd);
				 } else if(type.equals(type_list)){
					 this.set(key, list, secd);
				 }
			 } else {
				 if(type.equals(type_map)) {
					 this.set(key, map);
				 } else if(type.equals(type_list)){
					 this.set(key, list);
				 }

			 }
		 }
	 }
		
	public String tryLock(String key) {
    	Jedis jedis = getJedis();
	    try {
	        String value = fetchLockValue();
	        Long firstTryTime = System.currentTimeMillis();
	        do {
	            if (SET_SUCCESS.equals(jedis.set(key, value, "NX", "EX", lockExpirseTime))) {
	                return value;
	            }
	            try {
	                Thread.sleep(100);
	            } catch (InterruptedException e) {
	                e.printStackTrace();
	            }
	        } while ((System.currentTimeMillis() - tryExpirseTime * 1000) < firstTryTime);
	    } catch (Exception e) {
	        e.printStackTrace();
	    } finally {
	        if (jedis != null) {
	        	closeJedis(jedis);
	        }
	    }
	    return null;
	}

	public boolean unLock(String key,String value){
		Jedis jedis = getJedis();
		try {
			String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
			Object result = jedis.eval(script, Collections.singletonList(key), Collections.singletonList(value));
			if (RELEASE_SUCCESS.equals(result)) {
			    return true;
			}
			return false;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
	        if (jedis != null) {
	        	closeJedis(jedis);
	        }
		}
		return false;

	}
	
	private String fetchLockValue() {
	    return UUID.randomUUID().toString() + "_" + df.format(new Date());
	}
	
}