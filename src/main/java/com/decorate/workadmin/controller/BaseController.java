package com.decorate.workadmin.controller;

import com.decorate.workadmin.base.Response;
import com.decorate.workadmin.domain.vo.LoginVo;
import com.decorate.workadmin.exception.BusinessException;
import com.decorate.workadmin.utils.CommonUtils;
import com.decorate.workadmin.utils.ConstantsUtil;
import com.decorate.workadmin.utils.TokenUtil;
import com.decorate.workadmin.utils.enums.RequestCodeEnums;
import com.decorate.workadmin.utils.redis.RedisManager;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class BaseController {

	private static final Logger _logger = LoggerFactory.getLogger(BaseController.class);

	@Autowired
	public HttpServletRequest request;

	private static final  String LOGINDATE_CODE = RequestCodeEnums.LOGIN_EXPIRE.getCode();
	private static final  String LOGINDATE_MESSAGE = RequestCodeEnums.LOGIN_EXPIRE.getMsg();
	private static final  String SUCCESS_MESSAGE = RequestCodeEnums.SUCCESS.getMsg();
	private static final  String FAILED_MESSAGE = RequestCodeEnums.FAILED.getMsg();

	/**
	 * @title :	请求成功
	 * @author: tangxu
	 * @date: 2017年7月31日
	 * @param message
	 * @return
	 */
	public Response succResponse(final String message) {
		return Response.success(message);
	}
	public Response succResponse(final Object objData) {
		return Response.success(SUCCESS_MESSAGE, objData);
	}
	public Response succResponse(final String message, final Object objData) {
		return Response.success(message, objData);
	}

	
	/**
	 * @title :	请求结果失败（请求成功，结果失败）
	 * @author: tangxu
	 * @date: 2017年7月31日
	 * @param message
	 * @return
	 */
	public Response businessFail(final String resMsg) {
		if (StringUtils.contains(resMsg, LOGINDATE_MESSAGE)) {
			return Response.loginDate(resMsg, "");
		}
		return Response.fail(resMsg);
	}

	public Response failResponse(final String message) {
		return Response.fail(message);
	}
	public Response failResponse(final Object objData) {
		return Response.fail(FAILED_MESSAGE,objData);
	}
	
	public Response failResponse(final String message,final Object objData) {
		return Response.fail(message, objData);
	}
	
	/**
	 * @title :	请求异常（请求出现未知错误）
	 * @author: tangxu
	 * @date: 2017年7月31日
	 * @param resMsg
	 * @return
	 */
	public Response errorResponse(final String resMsg) {
		return Response.fail(resMsg);
	}
	
	public Response errorResponse(final String resMsg,Object obj) {
		if (StringUtils.contains(resMsg, LOGINDATE_MESSAGE)) {
			return Response.loginDate(resMsg, "");
		}
		return Response.fail(resMsg,obj);
	}


	/**
	 * 判断字符串是否为空
	 * @Title: isBlank
	 * @author jianghaoming
	 * @date 2016年9月14日  下午12:14:32
	 * @param str
	 * @return boolean 返回类型
	 * @des	StringUtils.isBlank(null)      = true
	 *		StringUtils.isBlank("")        = true
	 *		StringUtils.isBlank(" ")       = true
	 *		StringUtils.isBlank("bob")     = false
	 *		StringUtils.isBlank("  bob  ") = false
	 */
	public boolean isBlank(final String str) {
		if ("null".equals(str)) {
			return true;
		}
		return StringUtils.isBlank(str);
	}

	/**
	 * @Title: 获取参数
	 * @author jianghaoming
	 * @date 2016/10/17  10:47
	 * @param paramMap 参数map
	 * @param key 参数key
	 * @return 
	 */
	public String getValue(Map<String, Object> paramMap, String key) {
		return CommonUtils.getValue(paramMap, key);
	}

	/**
	 * @Title: 校验参数为空
	 * @author jianghaoming
	 * @date 2016/9/21  9:54
	 * @param  param 参数，message 提示信息
	 */
	public static void checkParamNull(final Object param, final String message) throws BusinessException {
		if( param == null ){
			throw new BusinessException(message);
		}
	}

	public String getValueAndCheckNull(Map<String, Object> paramMap, String key) throws BusinessException {
		final String paramStr = CommonUtils.getValue(paramMap, key);
		checkParamNull(paramStr, key + "不能为空");
		return paramStr;
	}

	/**
	 * 获取Ip地址
	 * @param request
	 * @return
	 */
	public static String getIpAdrress(HttpServletRequest request) {
		String Xip = request.getHeader("X-Real-IP");
		String XFor = request.getHeader("X-Forwarded-For");
		if(StringUtils.isNotEmpty(XFor) && !"unKnown".equalsIgnoreCase(XFor)){
			//多次反向代理后会有多个ip值，第一个ip才是真实ip
			int index = XFor.indexOf(",");
			if(index != -1){
				return XFor.substring(0,index);
			}else{
				return XFor;
			}
		}
		XFor = Xip;
		if(StringUtils.isNotEmpty(XFor) && !"unKnown".equalsIgnoreCase(XFor)){
			return XFor;
		}
		if (StringUtils.isBlank(XFor) || "unknown".equalsIgnoreCase(XFor)) {
			XFor = request.getHeader("Proxy-Client-IP");
		}
		if (StringUtils.isBlank(XFor) || "unknown".equalsIgnoreCase(XFor)) {
			XFor = request.getHeader("WL-Proxy-Client-IP");
		}
		if (StringUtils.isBlank(XFor) || "unknown".equalsIgnoreCase(XFor)) {
			XFor = request.getHeader("HTTP_CLIENT_IP");
		}
		if (StringUtils.isBlank(XFor) || "unknown".equalsIgnoreCase(XFor)) {
			XFor = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (StringUtils.isBlank(XFor) || "unknown".equalsIgnoreCase(XFor)) {
			XFor = request.getRemoteAddr();
		}
		return XFor;
	}

	/**
	 * @Description : 获取POST过来反馈信息
	 * @Param: []
	 * @Return : java.util.Map<java.lang.String,java.lang.Object>
	 * @Date : 2020/5/11
	*/
	public Map<String, Object> getPostParameterMap() {
		Map<?, ?> requestParams = request.getParameterMap();
		Map<String, Object> params = new HashMap<>();
		for (Iterator<?> iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
						: valueStr + values[i] + ",";
			}
			//valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8"); //乱码解决，这段代码在出现乱码时使用。
			params.put(name, valueStr);
		}
		return params;
	}

	/**
	 * 创建token
	 * @return : java.lang.String
	 * @date : 2020/11/10
	*/
	public String genetateToken(){
		return TokenUtil.genetateToken();
	}

	public String getTokenId() {
		String token =  request.getHeader(ConstantsUtil.Header.token);
		if(isBlank(token)){
			token = request.getHeader(ConstantsUtil.Header.accessToken);
			if(isBlank(token)){
				token = request.getParameter(ConstantsUtil.Header.accessToken);
			}
		}
		return token;
	}
	/**
	 * 查询登录信息
	 * @return : com.decorate.workadmin.domain.vo.LoginVo
	 * @date : 2020/11/10
	*/
	public LoginVo getLoginInfo() throws BusinessException, Exception {
		String token = this.getTokenId();
		if (StringUtils.isBlank(token)) {
			throw new BusinessException(LOGINDATE_CODE, "token不能为空");
		}
		return this.getLoginInfoByToken(token);
	}
	/**
	 * @title : 根据传入token查询登录信息
	 * @return
	 * @throws BusinessException
	 * @throws Exception
	 */
	public LoginVo getLoginInfoByToken(String token) throws BusinessException, Exception {
		if (StringUtils.isBlank(token)) {
			throw new BusinessException(LOGINDATE_CODE, "token不能为空");
		}
		LoginVo loginInfo = (RedisManager.getRedis().getObject(ConstantsUtil.CacheKey.LOGIN_TOKEN + token));
		if (null == loginInfo) {
//			CustomerInfo qryCustomerInfo = customerInfoService.queryByToken(token);
//			if (null != qryCustomerInfo) {
//				CustomerInfo = qryCustomerInfo;
//				RedisManager.getRedis().set(ConstantsUtil.CacheKey.LOGIN_TOKEN + token, CustomerInfo);
//			} else {
//				throw new BusinessException(_loginDateCode, ConstantsUtil.CommonMessage.LOGIN_DATE);
//			}
		}
		return loginInfo;
	}
}
