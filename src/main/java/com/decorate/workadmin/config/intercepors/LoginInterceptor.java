package com.decorate.workadmin.config.intercepors;

import com.alibaba.fastjson.JSON;
import com.decorate.workadmin.base.Response;
import com.decorate.workadmin.domain.vo.LoginVo;
import com.decorate.workadmin.utils.ConstantsUtil;
import com.decorate.workadmin.utils.enums.RequestCodeEnums;
import com.decorate.workadmin.utils.redis.RedisManager;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author : LCF
 * @title : 登录拦截器
 * @date : 2020/11/12
 */

@Component
public class LoginInterceptor implements HandlerInterceptor {
    /**
     * logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginInterceptor.class);

    /**
     * 这个方法是在访问接口之前执行的，我们只需要在这里写验证登陆状态的业务逻辑，就可以在用户调用指定接口之前验证登陆状态了
     *
     * @date : 2020/11/12
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        LOGGER.info("====================[LoginInterceptor.preHandle-->start]====================");
        //每一个项目对于登陆的实现逻辑都有所区别，这里使用token提取用户信息来验证登陆。
        String token = request.getHeader(ConstantsUtil.Header.token);
        if (StringUtils.isBlank(token)) {
            token = request.getHeader(ConstantsUtil.Header.accessToken);
            if (StringUtils.isBlank(token)) {
                token = request.getParameter(ConstantsUtil.Header.accessToken);
            }
        }
        LoginVo loginInfo = RedisManager.getRedis().getObject(ConstantsUtil.CacheKey.LOGIN_TOKEN + token);

        //如果缓存中没有用户，表示没登陆
        if (loginInfo == null) {
            //这个方法返回false表示忽略当前请求，如果一个用户调用了需要登陆才能使用的接口，如果他没有登陆这里会直接忽略掉
            //当然你可以利用response给用户返回一些提示信息，告诉他没登陆
            LOGGER.info("登录拦截：{}", RequestCodeEnums.LOGIN_EXPIRE.getMsg());
            returnJson(response, JSON.toJSONString(Response.loginDate(RequestCodeEnums.LOGIN_EXPIRE.getMsg(), "")));
            return false;
        } else {
            //如果缓存里有用户，表示该用户已经登陆，放行，用户即可继续调用自己需要的接口
            return true;
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {
    }

    /**
     * 返回客户端数据
     */
    private void returnJson(HttpServletResponse response, String result) throws Exception {
        PrintWriter writer = null;
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        try {
            writer = response.getWriter();
            writer.print(result);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }
}
