package com.decorate.workadmin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.decorate.workadmin.mapper")
@SpringBootApplication
public class WorkadminApplication extends ServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(WorkadminApplication.class, args);
	}

}
